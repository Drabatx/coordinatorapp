package com.drabatx.coordinatormap.ui.other.presentation.view

import com.drabatx.coordinatormap.models.response.Coord

interface OtherView {
    fun showProgress(show:Boolean)
    fun showMessage(message: String)
    fun showCoords(list: List<Coord>)
}