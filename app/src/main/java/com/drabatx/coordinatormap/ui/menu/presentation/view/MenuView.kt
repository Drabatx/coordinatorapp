package com.drabatx.coordinatormap.ui.menu.presentation.view

interface MenuView {
    fun showProgress(show:Boolean)
    fun showMessage(message: String)

}