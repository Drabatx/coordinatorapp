package com.drabatx.coordinatormap.ui.map.domain

interface MapInteractor {
    fun getLastCoord()
}