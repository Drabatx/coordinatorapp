package com.drabatx.coordinatormap.ui.other.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.drabatx.coordinatormap.databinding.FragmentOtherBinding
import com.drabatx.coordinatormap.models.response.Coord
import com.drabatx.coordinatormap.ui.dialogs.LoadingDialog
import com.drabatx.coordinatormap.ui.other.presentation.adapter.CoordAdapter
import com.drabatx.coordinatormap.ui.other.presentation.presenter.OtherPresenter
import com.drabatx.coordinatormap.ui.other.presentation.presenter.OtherPresenterImpl
import com.drabatx.coordinatormap.ui.other.presentation.view.OtherView
import com.google.android.material.snackbar.Snackbar

class OtherFragment : Fragment(), OtherView {
    private var _binding: FragmentOtherBinding? = null
    private val binding get() = _binding!!
    private var myAdapter: CoordAdapter?=null
    lateinit var loadingDialog: LoadingDialog

    lateinit var presenter: OtherPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOtherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = LoadingDialog()
        presenter =OtherPresenterImpl(this)
        initRecyclerView()
        presenter.getAllCoords()
    }



    override fun showProgress(show: Boolean) {
        if (show){
            loadingDialog.show(childFragmentManager, "loading dialog")
        }else{
            loadingDialog.dismiss()
        }

    }

    override fun showMessage(message: String) {
        Snackbar.make(binding.rvCoord,message, Snackbar.LENGTH_SHORT).show()
    }

    fun initRecyclerView(){
        myAdapter = CoordAdapter(arrayListOf())
        binding.rvCoord.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }
    }

    override fun showCoords(list: List<Coord>) {
        if (list.isNotEmpty()){
            myAdapter = CoordAdapter(list)
            binding.rvCoord.adapter = myAdapter
            myAdapter?.notifyDataSetChanged()
        }else{
            presenter.showMessage("No hay datos")
        }
    }
}