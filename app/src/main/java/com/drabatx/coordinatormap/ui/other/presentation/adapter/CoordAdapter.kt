package com.drabatx.coordinatormap.ui.other.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.drabatx.coordinatormap.R
import com.drabatx.coordinatormap.models.response.Coord

class CoordAdapter(private val list: List<Coord>) :
    RecyclerView.Adapter<CoordAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvLat: TextView
        val tvLon: TextView

        init {
            tvLat = view.findViewById(R.id.tvLatitudVal)
            tvLon = view.findViewById(R.id.tvLongitudVal)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_coord, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvLat.text = list[position].lat.toString()
        holder.tvLon.text = list[position].lon.toString()
    }

    override fun getItemCount(): Int {
        return list.size
    }
}