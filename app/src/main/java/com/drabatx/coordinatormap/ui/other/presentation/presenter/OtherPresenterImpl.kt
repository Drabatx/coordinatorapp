package com.drabatx.coordinatormap.ui.other.presentation.presenter

import com.drabatx.coordinatormap.models.response.Coord
import com.drabatx.coordinatormap.ui.other.domain.OtherInteractor
import com.drabatx.coordinatormap.ui.other.domain.OtherInteractorImpl
import com.drabatx.coordinatormap.ui.other.presentation.view.OtherView

class OtherPresenterImpl(val view: OtherView) : OtherPresenter {
    var interacor: OtherInteractor
    override fun getAllCoords() {
        interacor.getAllCoords()
    }

    override fun showProgress(show: Boolean) {
        view.showProgress(show)
    }

    override fun showMessage(message: String) {
        view.showMessage(message)
    }

    override fun showCoords(list: List<Coord>) {
        view.showCoords(list)
    }

    init {
        interacor = OtherInteractorImpl(this)
    }
}