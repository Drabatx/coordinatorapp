package com.drabatx.coordinatormap.ui.menu.presentation.presenter

interface MenuPresenter {
    fun getWeather()
    fun showProgress(show:Boolean)
    fun showMessage(message: String)
}