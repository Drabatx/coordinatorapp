package com.drabatx.coordinatormap.ui.menu.domain

import android.util.Log
import com.drabatx.coordinatormap.application.MyApp
import com.drabatx.coordinatormap.models.response.WeatherResponse
import com.drabatx.coordinatormap.models.db.AppDatabase
import com.drabatx.coordinatormap.models.db.CoordDao
import com.drabatx.coordinatormap.service.ApiService
import com.drabatx.coordinatormap.service.RetrofitBuilder
import com.drabatx.coordinatormap.ui.menu.presentation.presenter.MenuPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MenuInteractorImpl(val presenter : MenuPresenter) : MenuInteractor {
    val service: ApiService
    lateinit var call: Call<WeatherResponse>
    var coordDao: CoordDao

    override fun getWeather() {
        call = service.getInfo()
        presenter.showProgress(true)
        call.enqueue(object : Callback<WeatherResponse> {
            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                presenter.showProgress(false)
                if (response.isSuccessful) {
                    GlobalScope.launch(Dispatchers.IO){
                        this?.let { response.body()?.coord?.let { it1 -> coordDao.put(it1) } }
                    }
                    presenter.showMessage("Su solicitu se proceso correctamente")
                } else {
                    presenter.showMessage("Error al procesar tu solicitud")
                }
            }

            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                presenter.showProgress(false)
                presenter.showMessage("Error al enviar tu solicitud")
                Log.e(TAG, "onFailure: ", t.cause)
            }
        })
    }

    init {
        service = RetrofitBuilder.createService(ApiService::class.java)
        coordDao = MyApp.myDb().coordDao()
    }

    companion object {
        private const val TAG = "MenuInteractorImpl"
    }
}