package com.drabatx.coordinatormap.ui.menu.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.drabatx.coordinatormap.R
import com.drabatx.coordinatormap.databinding.FragmentMenuBinding
import com.drabatx.coordinatormap.ui.dialogs.LoadingDialog
import com.drabatx.coordinatormap.ui.menu.presentation.presenter.MenuPresenter
import com.drabatx.coordinatormap.ui.menu.presentation.presenter.MenuPresenterImpl
import com.drabatx.coordinatormap.ui.menu.presentation.view.MenuView
import com.google.android.material.snackbar.Snackbar

class MenuFragment : Fragment(), MenuView {

    private var _binding: FragmentMenuBinding? = null

    private val binding get() = _binding!!
    private lateinit var presenter: MenuPresenter
    lateinit var loadingDialog: LoadingDialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        presenter = MenuPresenterImpl(this)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadingDialog = LoadingDialog()

        binding.btnShowMap.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_FirstFragment_to_mapsFragment)
        }
        binding.btnShowList.setOnClickListener {
            binding.btnShowList.isEnabled = false
            Navigation.findNavController(view).navigate(R.id.action_FirstFragment_to_otherFragment)
        }
        binding.btnSendRequest.setOnClickListener {
            presenter.getWeather()
        }
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            loadingDialog.show(childFragmentManager, "Loading dialog")
        } else {
            loadingDialog.dismiss()
        }
    }

    override fun showMessage(message: String) {
        Snackbar.make(binding.btnSendRequest, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val TAG = "MenuFragment"
    }
}