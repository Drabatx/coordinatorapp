package com.drabatx.coordinatormap.ui.map.presentation.view

import com.drabatx.coordinatormap.models.response.Coord

interface MapsView {
    fun showProgress(show:Boolean)
    fun showMessage(message: String)
    fun showCoord(coord: Coord)
}