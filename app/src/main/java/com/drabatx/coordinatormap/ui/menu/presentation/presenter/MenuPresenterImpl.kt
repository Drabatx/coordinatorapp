package com.drabatx.coordinatormap.ui.menu.presentation.presenter

import com.drabatx.coordinatormap.ui.menu.domain.MenuInteractor
import com.drabatx.coordinatormap.ui.menu.domain.MenuInteractorImpl
import com.drabatx.coordinatormap.ui.menu.presentation.view.MenuView

class MenuPresenterImpl(val view: MenuView) : MenuPresenter {
    val interactor: MenuInteractor
    override fun getWeather() {
        interactor.getWeather()
    }

    override fun showProgress(show: Boolean) {
        view.showProgress(show)
    }

    override fun showMessage(message: String) {
        view.showMessage(message)
    }

    init {
        interactor = MenuInteractorImpl(this)
    }
}