package com.drabatx.coordinatormap.ui.map.presentation.presenter

import com.drabatx.coordinatormap.models.response.Coord
import com.drabatx.coordinatormap.ui.map.domain.MapInteractor
import com.drabatx.coordinatormap.ui.map.domain.MapInteractorImpl
import com.drabatx.coordinatormap.ui.map.presentation.view.MapsView

class MapPresenterImpl(val view: MapsView): MapPresenter {
    val interactor: MapInteractor
    override fun showProgress(show: Boolean) {
        view.showProgress(show)
    }

    override fun showMessage(message: String) {
        view.showMessage(message)
    }

    override fun showCoord(coord: Coord) {
        view.showCoord(coord)
    }

    override fun getLastCoord() {
        interactor.getLastCoord()
    }

    init {
        interactor = MapInteractorImpl(this)
    }
}