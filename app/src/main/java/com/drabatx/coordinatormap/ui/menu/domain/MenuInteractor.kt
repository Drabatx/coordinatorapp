package com.drabatx.coordinatormap.ui.menu.domain


interface MenuInteractor {
    fun getWeather()
}