package com.drabatx.coordinatormap.ui.other.domain

import com.drabatx.coordinatormap.models.response.Coord

interface OtherInteractor {
    fun getAllCoords()
}