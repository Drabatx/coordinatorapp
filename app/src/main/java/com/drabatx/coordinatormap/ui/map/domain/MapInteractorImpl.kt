package com.drabatx.coordinatormap.ui.map.domain

import com.drabatx.coordinatormap.application.MyApp
import com.drabatx.coordinatormap.models.db.AppDatabase
import com.drabatx.coordinatormap.models.db.CoordDao
import com.drabatx.coordinatormap.ui.map.presentation.presenter.MapPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MapInteractorImpl(val presenter: MapPresenter) : MapInteractor {
    val coordDao: CoordDao
    override fun getLastCoord() {
        presenter.showProgress(true)
        GlobalScope.launch(Dispatchers.IO) {
            presenter.showProgress(false)
            presenter.showCoord(coordDao.pop())
        }
    }

    init {
        coordDao =MyApp.myDb().coordDao()
    }

    companion object {
        private const val TAG = "MapInteractorImpl"
    }
}