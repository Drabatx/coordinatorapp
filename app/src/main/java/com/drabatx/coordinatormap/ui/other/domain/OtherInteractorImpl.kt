package com.drabatx.coordinatormap.ui.other.domain

import com.drabatx.coordinatormap.application.MyApp
import com.drabatx.coordinatormap.models.db.CoordDao
import com.drabatx.coordinatormap.models.response.Coord
import com.drabatx.coordinatormap.ui.other.presentation.presenter.OtherPresenter

class OtherInteractorImpl(val presenter: OtherPresenter) : OtherInteractor {
    var coordDao: CoordDao = MyApp.myDb().coordDao()

    var list: List<Coord>? = null
    override fun getAllCoords() {
        presenter.showProgress(true)
        presenter.showCoords(coordDao.getAll().reversed())
        presenter.showProgress(false)

//        GlobalScope.launch (Dispatchers.Main){
//            presenter.showProgress(true)
//        }
//        GlobalScope.launch(Dispatchers.IO) {
//            list = coordDao.getAll()
//            launch(Dispatchers.Main) {
//                presenter.showProgress(false)
//                list?.let {
//                    presenter.showCoords(it.reversed())
//                }
//            }
//        }
    }

}