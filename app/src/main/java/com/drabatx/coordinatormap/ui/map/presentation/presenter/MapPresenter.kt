package com.drabatx.coordinatormap.ui.map.presentation.presenter

import com.drabatx.coordinatormap.models.response.Coord

interface MapPresenter {
    fun showProgress(show:Boolean)
    fun showMessage(message: String)
    fun showCoord(coord: Coord)

    fun getLastCoord()
}