package com.drabatx.coordinatormap.ui.map.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.drabatx.coordinatormap.R
import com.drabatx.coordinatormap.databinding.FragmentMapsBinding
import com.drabatx.coordinatormap.models.response.Coord
import com.drabatx.coordinatormap.ui.dialogs.LoadingDialog
import com.drabatx.coordinatormap.ui.map.presentation.presenter.MapPresenter
import com.drabatx.coordinatormap.ui.map.presentation.presenter.MapPresenterImpl
import com.drabatx.coordinatormap.ui.map.presentation.view.MapsView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MapsFragment : Fragment(), OnMapReadyCallback, MapsView {

    private var _binding: FragmentMapsBinding? = null

    private val binding get() = _binding!!

    lateinit var mMap: GoogleMap
    lateinit var mMapFragment: SupportMapFragment

    lateinit var loadingDialog: LoadingDialog
    lateinit var presenter: MapPresenter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMapsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        loadingDialog = LoadingDialog()
        presenter = MapPresenterImpl(this)
        mMapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        presenter.getLastCoord()

//        val nCood = LatLng(12.54, 127.04)
//        mMap.addMarker(MarkerOptions().position(nCood).title("Coord here"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(nCood))
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            loadingDialog.show(childFragmentManager, "loading dialog")
        } else {
            loadingDialog.dismiss()
        }
    }

    override fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()
    }

    override fun showCoord(coord: Coord) {
        GlobalScope.launch(Dispatchers.Main) {
            val nCood = LatLng(coord.lat!!, coord.lon!!)
            mMap.addMarker(MarkerOptions().position(nCood).title("Coord here"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(nCood))
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    nCood, 12.0f
                )
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}