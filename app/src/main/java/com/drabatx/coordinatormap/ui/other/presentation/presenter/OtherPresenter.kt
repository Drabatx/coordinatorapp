package com.drabatx.coordinatormap.ui.other.presentation.presenter

import com.drabatx.coordinatormap.models.response.Coord

interface OtherPresenter {
    fun getAllCoords()
    fun showProgress(show:Boolean)
    fun showMessage(message: String)
    fun showCoords(list: List<Coord>)
}