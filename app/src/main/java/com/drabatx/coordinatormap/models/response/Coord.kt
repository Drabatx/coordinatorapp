package com.drabatx.coordinatormap.models.response

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Coord(
	@PrimaryKey(autoGenerate = true) val uid: Int,
	@ColumnInfo(name = "latitud")
	val lon: Double? = null,
	@ColumnInfo(name = "longitud")
	val lat: Double? = null
){

}