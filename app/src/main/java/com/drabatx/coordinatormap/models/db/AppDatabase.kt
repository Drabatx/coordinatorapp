package com.drabatx.coordinatormap.models.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.drabatx.coordinatormap.models.response.Coord

@Database(entities = [Coord::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun coordDao(): CoordDao

    companion object {
        private var db_instance: AppDatabase? = null
        fun getAppDatabaseInstace(context: Context): AppDatabase {
            if (db_instance == null) {
                db_instance = Room.databaseBuilder(context, AppDatabase::class.java, "db_coord")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return db_instance!!
        }
    }
}