package com.drabatx.coordinatormap.models.response

data class Wind(
	val deg: Int? = null,
	val speed: Double? = null,
	val gust: Double? = null
)