package com.drabatx.coordinatormap.models.response

data class Rain(
	val jsonMember1h: Double? = null
)