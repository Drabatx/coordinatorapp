package com.drabatx.coordinatormap.models.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.drabatx.coordinatormap.models.response.Coord

@Dao
interface CoordDao {
    @Query("select * from coord")
    fun getAll(): List<Coord>

    @Query("select * from coord order by uid desc limit 1")
    fun pop(): Coord

    @Insert
    fun put(vararg coord: Coord)

    @Delete
    fun delete(coord: Coord)
}