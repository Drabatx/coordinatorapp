package com.drabatx.coordinatormap.service

import com.drabatx.coordinatormap.models.response.WeatherResponse
import retrofit2.Call
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("weather?q=London%2Cuk&lat=0&lon=0&id=2172797&lang=null&units=%22metric%22%20or%20%22imperial%22&mode=xml%2C%20html")
    fun getInfo(): Call<WeatherResponse>
}