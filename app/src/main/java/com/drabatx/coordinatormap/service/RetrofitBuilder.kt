package com.drabatx.coordinatormap.service

import com.drabatx.coordinatormap.BuildConfig
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitBuilder {

    companion object {
        private val client = buildClient()
        private val retrofit = buildRetrofit(client!!)

        private fun buildClient(): OkHttpClient? {
            val interceptor = HttpLoggingInterceptor()
            val builder: OkHttpClient.Builder = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    var request: Request = chain.request()
                    val builder1: Request.Builder = request.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("x-rapidapi-key","d3fe3a34c5mshf70e1bb90c464a8p1ba4aajsn4233972e8a9c")
                        .addHeader("x-rapidapi-host","community-open-weather-map.p.rapidapi.com")
                    request = builder1.build()
                    chain.proceed(request)
                }
                .callTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                builder.addNetworkInterceptor(StethoInterceptor())
            }
            builder.retryOnConnectionFailure(true)
            return builder.build()
        }

        private fun buildRetrofit(client: OkHttpClient): Retrofit? {
            return Retrofit.Builder()
                .baseUrl("https://community-open-weather-map.p.rapidapi.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        fun <T> createService(service: Class<T>?): T {
            return retrofit!!.create(service)
        }
        fun getRetrofit(): Retrofit? {
            return retrofit
        }

    }
}