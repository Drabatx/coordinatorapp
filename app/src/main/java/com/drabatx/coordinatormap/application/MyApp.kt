package com.drabatx.coordinatormap.application

import android.app.Application
import com.drabatx.coordinatormap.models.db.AppDatabase
import com.facebook.stetho.Stetho

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this);
        db = AppDatabase.getAppDatabaseInstace(this)
    }


    companion object {
        lateinit var db: AppDatabase
        fun myDb(): AppDatabase {
            return db
        }

    }
}